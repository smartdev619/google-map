      var poly;
      var map;
      var locations = [];
      var labelIndex = 1;
      var markersArray = {};
      var baseurl = document.getElementById('baseurl').value;
      var geocoder;
      var startImage = baseurl + 'img/s.png';
      var finishImage = baseurl + 'img/finish.png';
      var startFinishImage = baseurl + 'img/start-finish.png';
      var start_finish;
      var start, finish;
      var start_drag_lat, start_drag_lng;
      var end_drag;
      var path,type;


      function initMap() {
          map = new google.maps.Map(document.getElementById('map'), {
              zoom: 7,
              center: {
                  lat: 41.879,
                  lng: -87.624
              } // Center the map on Chicago, USA.
          });

          start = {
              url: startImage,
              origin: new google.maps.Point(0, -25)
          };
          finish = {
              url: finishImage,
              origin: new google.maps.Point(0, -25)
          };
          start_finish = {
              url: startFinishImage,
              origin: new google.maps.Point(0, -25)
          };

          poly = new google.maps.Polyline({
              strokeColor: '#FF0000',
              strokeOpacity: 1.0,
              strokeWeight: 3,
              geodesic:true
          });
          poly.setMap(map);

          geocoder = new google.maps.Geocoder();
          /** Autocomplete */
          var autocomplete = new google.maps.places.Autocomplete(document.getElementById('pac-input'));
          autocomplete.bindTo('bounds', map);

          var infowindow = new google.maps.InfoWindow();
          var marker = new google.maps.Marker({
              map: map,
              anchorPoint: new google.maps.Point(0, -29)
          });

          autocomplete.addListener('place_changed', function() {
              infowindow.close();
              marker.setVisible(false);
              var place = autocomplete.getPlace();
              if (!place.geometry) {
                  window.alert("Autocomplete's returned place contains no geometry");
                  return;


              }

              // If the place has a geometry, then present it on a map.
              if (place.geometry.viewport) {
                  map.fitBounds(place.geometry.viewport);
              } else {
                  map.setCenter(place.geometry.location);
                  map.setZoom(17);
              }

              marker.setIcon({
                  url: place.icon,
                  size: new google.maps.Size(71, 71),
                  origin: new google.maps.Point(0, 0),
                  anchor: new google.maps.Point(17, 34),
                  scaledSize: new google.maps.Size(35, 35)
              });

          });

          /***********************/

          // Add a listener for the click event
          map.addListener('click', addLatLng);
      }

       // Handles click events on a map, and adds a new point to the Polyline.
      function addLatLng(event) {
          path = poly.getPath();

          // Because path is an MVCArray, we can simply append a new coordinate
          // and it will automatically appear.
          path.push(event.latLng);
          var length = path.getLength();

          // Add a new marker at the new plotted point on the polyline.

          if (length == 1) {
            type = $('#checkfinal').val();
            if(type == 's'){
              var iconsT = start;
            }else{
              var iconsT = start_finish
            }


              geocoder.geocode({
                      'latLng': event.latLng
                  },
                  function(results, status) {
                      if (status == google.maps.GeocoderStatus.OK) {
                          if (results[0]) {
                              var add = results[0].formatted_address;
                              var value = add.split(",");
                              count = value.length;
                              country = value[count - 1];
                              state = value[count - 2];
                              city = value[count - 3];
                              document.getElementById('pac-input').value = city + state;

                          }
                      } else {
                          alert("Geocoder failed due to: " + status);
                      }
                  }
              );

              var marker = new MarkerWithLabel({
                  position: event.latLng,
                  map: map,
                  labelClass: "start", // the CSS class for the label
                  labelInBackground: false,
                  icon: iconsT,
                  draggable: true
              });

          } else {
             
              var marker = new MarkerWithLabel({
                  position: event.latLng,
                  map: map,
                  labelContent: String(labelIndex++),
                  labelAnchor: new google.maps.Point(15, 40),
                  labelClass: "labels", // the CSS class for the label
                  labelInBackground: false,
                  draggable: true,
                  icon: {
                      path: google.maps.SymbolPath.CIRCLE,
                      scale: 10,
                      strokeColor: 'red',
                      strokeWeight: 2
                  }
              });
              type="c";


          }
          markersArray[labelIndex] = marker;
       //   bindMarkerEvents(marker);

          var dragstart = markersArray[labelIndex].addListener('dragstart', startDrag);
          var dragend = markersArray[labelIndex].addListener('dragend', endDrag);

          function startDrag(event) {
              start_drag_lat = event.latLng.lat();
              start_drag_lng = event.latLng.lng();


          }

          function endDrag(event) {

              $.each(locations, function(indexl, vall) {
                  if (vall != undefined) {
                      if (vall.lat == start_drag_lat) {
                          path.removeAt(indexl);
                          path.insertAt(indexl, event.latLng);
                          locations[indexl].lat = event.latLng.lat();
                          locations[indexl].lon = event.latLng.lng();
                          var jsonlocations = JSON.stringify(locations);
                          document.getElementById('CoursePositions').value = jsonlocations;
                      }
                  }
              });
              var jsonlocations = JSON.stringify(locations);
              document.getElementById('CoursePositions').value = jsonlocations;

          }



          var lat = event.latLng.lat();
          var lng = event.latLng.lng();
          locations.push({
              lat: lat,
              lon: lng,
              controlId: labelIndex,
              type:type
          });
          var jsonlocations = JSON.stringify(locations);

          document.getElementById('CoursePositions').value = jsonlocations;
      }


      var bindMarkerEvents = function(marker) {
          google.maps.event.addListener(marker, "rightclick", function(point) {
              $.each(locations, function(index, val) {
                  if (val != undefined) {
                      if (val.lat == point.latLng.lat()) {
                          console.log(val);
                          var marker = markersArray[val.controlId]; // find marker
                          removeMarker(marker, val.controlId); // remove it
                      }
                  }

              });


          });
      };

      var removeMarker = function(marker, markerId) {
          marker.setMap(null); // set markers setMap to null to remove it from map
          delete markersArray[markerId]; // delete marker instance from markers object
          path.removeAt(markerId - 1);
          $.each(locations, function(index, val) {
              if (val != undefined) {
                  if (val.controlId == markerId) {
                      locations.splice(index, 1);
                  }
              }
          });

          var updatelocations = JSON.stringify(locations);
          document.getElementById('CoursePositions').value = updatelocations;
      };

      $('.score-control').click(function() {

          if ($(this).hasClass('active')) {
              $(this).removeClass('active');
              $.each(markersArray, function(index, val) {
                  if (markersArray[index].get('labelClass') == 'start') {
                      var markerfinal = markersArray[index]; // find the marker by given id
                      markerfinal.setIcon(start);
                  }
              });
               if(locations[0] != undefined){
                      locations[0].type = 's';
                  }
                      $('#checkfinal').val('s');

          } else {
              $(this).addClass('active');
              $.each(markersArray, function(index, val) {
                  if (markersArray[index].get('labelClass') == 'start') {
                      var markerfinal = markersArray[index]; // find the marker by given id
                      markerfinal.setIcon(start_finish);
                  }
              });
              if(locations[0] != undefined){
                locations[0].type = 'f';
              }
                $('#checkfinal').val('f');

          }
          var jsonlocations = JSON.stringify(locations);
          document.getElementById('CoursePositions').value = jsonlocations;
      });


      google.maps.event.addDomListener(window, 'load', initMap);
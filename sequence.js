var baseurl = document.getElementById('baseurl').value;
var markers = {};
var locations = [];
var startImage = baseurl + 'img/s.png';
var finishImage = baseurl + 'img/finish.png';
var startFinishImage = baseurl + 'img/start-finish.png';
var type = "s";
var start, finish, start_finish;
var labelIndex = 0;


function initMap() {
    var myLatlng = {
        lat: -25.363,
        lng: 131.044
    };


    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 10,
        center: myLatlng

    });
    var geocoder = new google.maps.Geocoder();

    var autocomplete = new google.maps.places.Autocomplete(document.getElementById('pac-input'));
    autocomplete.bindTo('bounds', map);

    var infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });

    autocomplete.addListener('place_changed', function() {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry");
            return;


        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }

        marker.setIcon({
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
        });

    });




    map.addListener('click', function(event) {

        placeMarker(event.latLng, map);

    });

    function placeMarker(location, map) {
        var checkControl = document.getElementById('check_control').value;

        start = {
            url: startImage,
            origin: new google.maps.Point(0, -25)
        };
        finish = {
            url: finishImage,
            origin: new google.maps.Point(0, -25)
        };
        start_finish = {
            url: startFinishImage,
            origin: new google.maps.Point(0, -25)
        };



        if (checkControl == 'startButton') {

            geocoder.geocode({
                    'latLng': location
                },
                function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            var add = results[0].formatted_address;
                            var value = add.split(",");
                            count = value.length;
                            country = value[count - 1];
                            state = value[count - 2];
                            city = value[count - 3];
                            document.getElementById('pac-input').value = city + state;

                        }
                    } else {
                        alert("Geocoder failed due to: " + status);
                    }
                }
            );

            type = "s";

            $.each(locations, function(index, val) { // Remove Previous starting point
                if (val != undefined) {
                    if (val.type == 's') {
                        locations.splice(index, 1);
                    }
                }

            });

            $.each(markers, function(index, val) {
                if (markers[index].get('labelClass') == 'start') {
                    removeMarker(markers[index], index, 's');
                }
            });

            var marker = new MarkerWithLabel({
                position: location,
                map: map,
                labelClass: "start", // the CSS class for the label
                labelInBackground: false,
                draggable: true,
                icon: start
            });
            locations.push({
                lat: location.lat(),
                lon: location.lng(),
                type: type,
                control: 0
            });

        } else if (checkControl == 'finishButton') {

            type = "f";

            $.each(locations, function(index, val) { // Remove Previous starting point
                if (val != undefined) {
                    if (val.type == 'f') {
                        locations.splice(index, 1);
                    }
                }

            });

            $.each(markers, function(index, val) {
                if (markers[index].get('labelClass') == 'finish') {
                    removeMarker(markers[index], index, 'f');
                }
            });

            var marker = new MarkerWithLabel({
                position: location,
                map: map,
                labelClass: "finish", // the CSS class for the label
                labelInBackground: false,
                draggable: true,
                icon: finish
            });
            locations.push({
                lat: location.lat(),
                lon: location.lng(),
                type: type,
                control: 200
            });

        } else {
            labelIndex++;
            var marker = new MarkerWithLabel({
                position: location,
                map: map,
                labelContent: String(labelIndex),
                labelAnchor: new google.maps.Point(15, 30),
                labelClass: "labels", // the CSS class for the label
                labelInBackground: false,
                draggable: true,
                icon: {
                    path: google.maps.SymbolPath.CIRCLE,
                    scale: 10,
                    strokeColor: 'red',
                    strokeWeight: 2

                }
            });
            type = "c";
            locations.push({
                lat: location.lat(),
                lon: location.lng(),
                type: type,
                control: labelIndex
            });


        }

        $('#totalControls').text(labelIndex);


        markers[labelIndex] = marker; // cache marker in markers object
        bindMarkerEvents(marker); // bind right click event to marker

        var dragstart = markers[labelIndex].addListener('dragstart', startDrag);
        var dragend = markers[labelIndex].addListener('dragend', endDrag);

        function startDrag(event) {
            start_drag_lat = event.latLng.lat();
            start_drag_lng = event.latLng.lng();


        }

        function endDrag(event) {

            var latdrag = event.latLng.lat();
            var londrag = event.latLng.lng();
            $.each(locations, function(index, val) {
                if (val.lat == start_drag_lat) {
                    locations[index].lat = latdrag;
                    locations[index].lon = londrag;
                }

            });
            var jsonlocations = JSON.stringify(locations);
            document.getElementById('CoursePositions').value = jsonlocations;

        }





        var jsonlocations = JSON.stringify(locations);
        document.getElementById('CoursePositions').value = jsonlocations;
    }


    var bindMarkerEvents = function(marker) {
        google.maps.event.addListener(marker, "rightclick", function(point) {
            $.each(locations, function(index, val) {
                if (val != undefined) {
                    if (val.lat == point.latLng.lat()) {
                        var marker = markers[val.control]; // find marker
                        removeMarker(marker, val.control); // remove it
                    }
                }

            });


        });
    };




}

$('.score-control').click(function(event) {
    var id = $(this).attr('id');
    $('#check_control').val(id);
    $('.score-control').removeClass('active');
    $(this).addClass('active');

    if (id == 'finishButton') {
        if (confirm('Is Start Location and Finish location are same?')) {

            $.each(markers, function(index, val) {
                if (markers[index].get('labelClass') == 'start') {
                    var markerfinal = markers[index]; // find the marker by given id
                    markerfinal.setIcon(start_finish);
                }

                if (markers[index].get('labelClass') == 'finish') {
                    removeMarker(markers[index], index, 'f');
                }
            });

            $('#check_control').val('controlButton');
            $('.score-control').removeClass('active');
            $('#controlButton').addClass('active');

        } else {
            $('#check_control').val(id);
            $('.score-control').removeClass('active');
            $('#finishButton').addClass('active');

            $.each(locations, function(index, val) { // Remove Previous starting point
                if (val != undefined) {
                    if (val.type == 's') {
                        var markerstart = markers[val.control]; // find the marker by given id
                        markerstart.setIcon(start);
                    }
                }

            });
        }
    }
});

var removeMarker = function(marker, markerId, type = null) {
    marker.setMap(null); // set markers setMap to null to remove it from map
    delete markers[markerId]; // delete marker instance from markers object

    $.each(locations, function(index, val) {
        if (type != 'f') {
            if (val != undefined) {
                if (val.control == markerId) {
                    locations.splice(index, 1);
                }

                if (val.type == 's') {
                    locations[index].control = 0;
                } else if (val.type == 'f') {
                    locations[index].control = 200;
                } else {
                    if (locations[index] != undefined) {
                        locations[index].control = parseInt(index);
                    }

                }
            }

        } else if (type == 'f') {

            if (val != undefined) {
                if (val.control == 200) {
                    locations.splice(index, 1);
                }

                if (val.type == 's') {
                    locations[index].control = 0;
                } else if (val.type == 'f') {
                    locations[index].control = 200;
                } else {
                    if (locations[index] != undefined) {
                        locations[index].control = parseInt(index);
                    }

                }
            }




        }
    });


    if (type != 's' && type != 'f') {
        labelIndex--;
        if (labelIndex < 0) {
            labelIndex = 0;
        }
    }
    $('#totalControls').text(labelIndex);
    var numberi = 1;
    $.each(markers, function(index, val) {

        if (markers[index].get('labelContent') != '') {
            markers[index].set('labelContent', numberi);
            numberi++;
        }
    });

    $.each(markers, function(index, val) {

        if (index > markerId) {
            markers[parseInt(index) - 1] = markers[index];
        }

    });
    var updatelocations = JSON.stringify(locations);
    document.getElementById('CoursePositions').value = updatelocations;

};
google.maps.event.addDomListener(window, 'load', initMap);

$('#scatterCreate').click(function(e) {
    if ($('#CourseRequiredControl').val() > parseInt($('#totalControls').text()) || $('#CourseRequiredControl').val() < 1) {
        $('.controlError').text('Enter Valid Controls');
        e.preventDefault();
    } else {
        $('.controlError').text('');
        $('#CourseCreateForm').submit();
    }
});